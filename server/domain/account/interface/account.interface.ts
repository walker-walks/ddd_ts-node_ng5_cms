import { User } from '../../+shared/entity/user.mapper';

export interface IAccount extends User {
  updateInfo(): void;
}
