import * as Bluebird from 'bluebird';
import { NumberID, StringID } from '../../+shared/types/generic.type';
import { Token } from 'typedi';
import { UserAccount } from '../entity/user-account.entity';
import { IAccount } from './account.interface';

export interface IAccountRepository {
  add(account: IAccount): Bluebird<void>;
  remove(account: IAccount): Bluebird<void>;
  // changePass(login: LoginInfo, newLoginInfo: LoginInfo): boolean;
  // update(user: User): boolean;
  findUserById(id: NumberID): Bluebird<UserAccount>;
  findUserByUsername(username: StringID): Bluebird<UserAccount>;
}

export const accountRepositoryService = new Token<IAccountRepository>();
