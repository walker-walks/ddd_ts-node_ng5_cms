
import { IBuildOptions } from 'sequelize-typescript';
import { IAccount } from '../interface/account.interface';
import { User } from '../../+shared/entity/user.mapper';
import { AccountType } from '../../+shared/types/account.type';


export class UserAccount extends User implements IAccount{
  protected type: AccountType = AccountType.USER;

  constructor(values?: any, options?: IBuildOptions | undefined) {
    super(values, options);
    // this.entity = new User();
  }

  updateInfo(): void {
    throw new Error('Method not implemented.');
  }
}

// export class TenantAccount implements IAccount{
//   id!: NumberID;
//   update(): void {
//     throw new Error('Method not implemented.');
//   }
//   private entity: User;
//   private type: AccountType = 'TENANT';
//   constructor() {
//     this.entity = new User();
//   }

// }

// export class VisitorAccount implements IAccount{
//   id!: NumberID;

//   update(): void {
//     throw new Error('Method not implemented.');
//   }
//   private entity: User;
//   private type: AccountType = 'VISITOR';
//   constructor() {
//     this.entity = new User();
//   }

// }

// export class MasterAccount implements IAccount{
//   id!: NumberID;

//   update(): void {
//     throw new Error('Method not implemented.');
//   }
//   private entity: User;
//   private type: AccountType = 'MASTER';
//   constructor() {
//     this.entity = new User();
//   }

// }
