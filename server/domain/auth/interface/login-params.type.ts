import { StringID } from '../../../+shared/types/generic.type';

export type Credentials = {
  username: string;
  password: string;
};
