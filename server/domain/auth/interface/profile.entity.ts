import { StringID } from '../../../+shared/types/generic.type';

export class Profile {
  address!: Address;

}

type Address = {
  country: string;
  zipCode: number;
  state: string;
  neighborhood: string;
  street: string;
  complements: string;
};
