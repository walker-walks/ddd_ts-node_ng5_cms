export type AccountType =
  'USER' | 'TENANT' | 'VISITOR' | 'MASTER';

export namespace AccountType {
  export const USER: AccountType = 'USER';
  export const TENANT: AccountType = 'TENANT';
  export const VISITOR: AccountType = 'VISITOR';
  export const MASTER: AccountType = 'MASTER';
}

