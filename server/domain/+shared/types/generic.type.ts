
export type StringID = string;
export type NumberID = number;
export type GenericID = StringID | NumberID;
