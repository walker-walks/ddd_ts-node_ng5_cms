import { Sequelize, ISequelizeConfig } from 'sequelize-typescript';
import { User } from './user.mapper';
import { Post } from './post.mapper';

export function dbConectionFactory(dbConfig: ISequelizeConfig) {
  // dbConfig.modelPaths = [__dirname + '/*.mapper.ts'];
  const sequelize = new Sequelize(dbConfig);
  sequelize.addModels([
    User,
    Post, 
  ]);
  return sequelize;
}
