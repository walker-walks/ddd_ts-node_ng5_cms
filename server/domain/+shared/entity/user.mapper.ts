import {
  Table, Column, PrimaryKey, Unique,
  Model, HasMany,
  CreatedAt, UpdatedAt, DeletedAt, DataType, Sequelize,
} from 'sequelize-typescript';
import { Post } from './post.mapper';
import { AccountType } from '../types/account.type';
import { NumberID, StringID } from '../types/generic.type';

@Table({
  timestamps: true,
})
export class User extends Model<User> {

  @PrimaryKey
  @Column(Sequelize.INTEGER(10))
  id?: NumberID;

  @Column(Sequelize.STRING(10))
  protected type: AccountType = 'USER';

  @Unique
  @Column(Sequelize.STRING(255))
  username?: StringID;

  @Column(Sequelize.STRING(255))
  password?: StringID;

  @Column
  nickname?: string;

  @CreatedAt
  creationDate?: Date;
 
  @UpdatedAt
  updatedOn?: Date;

  @HasMany(() => Post)
  posts?: Post[];

}
