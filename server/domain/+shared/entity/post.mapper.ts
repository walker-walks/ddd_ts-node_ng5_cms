import {
  Table, Column, PrimaryKey,
  Model, DataType,
  HasMany, BelongsTo,
  CreatedAt, UpdatedAt, DeletedAt, ForeignKey, Sequelize,
} from 'sequelize-typescript';
import { User } from './user.mapper';
import { NumberID } from '../types/generic.type';

@Table({
  timestamps: true,
})
export class Post extends Model<Post> {

  @PrimaryKey
  @Column(Sequelize.INTEGER)
  id?: NumberID;

  @Column
  title?: string;

  @Column(DataType.TEXT)
  body?: string;

  @CreatedAt
  creationDate?: Date;
 
  @UpdatedAt
  updatedOn?: Date;
  
  @DeletedAt
  deletionDate?: Date;

  // Relations
  @ForeignKey(() => User)
  authorId!: NumberID;
  @BelongsTo(() => User)
  author?: Post[];

}
