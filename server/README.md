# Getting Started

On Linux based systems:
```terminal
npm start
```

Windows: 
```terminal
npm start:win
```

## Rule
1. One Class max length 100 lines
1. One method until 5 lines
1. One method One indent
1. Never use "else"
1. Wrap all primitive types
1. 1 line until 2 dots
1. Never abbreviate names
1. Never alter propertie directly
1. One instance until 5 variables
1. One folder until 10 files


## patterns

### Parameter object
join the parameters into one object and move logic to there.

### Method Object
turn long method in to a object

## Object types
- Information Holder
- Coodinator
- Structure
- Controller
- Service Provider
- Interfacer