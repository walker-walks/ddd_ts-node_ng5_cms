import { JsonController, Get } from 'routing-controllers';

@JsonController('v1/_status/')
export class ServerManagerAPI {

  @Get('alive')
  alive() {
    return 'alive';
  }
}
