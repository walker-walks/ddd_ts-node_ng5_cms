import {
  JsonController,
  Post, Delete, Put, Get,
  Param, Body, BadRequestError, InternalServerError, ForbiddenError,
} from 'routing-controllers';
import { Inject } from 'typedi';
import { Credentials } from '../domain/auth/interface/login-params.type';
import { UserAccount } from '../domain/account/entity/user-account.entity';
import {
  accountRepositoryService,
  IAccountRepository,
} from '../domain/account/interface/account-repository.interface';
import { NumberID } from '../domain/+shared/types/generic.type';
import { AccountType } from '../domain/+shared/types/account.type';
import { ActionType } from 'routing-controllers/metadata/types/ActionType';
import * as Bluebird from 'bluebird';

@JsonController('v1/user/')
export class AccountManagerApp {
  private lookUpType: AccountType = AccountType.USER;
  private accountRepositories: { [t in AccountType]: IAccountRepository };
  private get accountRepo(): IAccountRepository {
    return this.accountRepositories[this.lookUpType];
  }

  constructor(
    @Inject(type => accountRepositoryService) userAccountRepo: IAccountRepository,
  ) {
    this.accountRepositories = {
      USER: userAccountRepo,
      TENANT: userAccountRepo,
      VISITOR: userAccountRepo,
      MASTER: userAccountRepo,
    };
  }

  @Post()
  createUser(@Body() credencials: Credentials): Bluebird<void> {
    const account = new UserAccount(credencials);
    return this.accountRepo.add(account)
    .catch(this.internalErrorHandler);
  }

  @Delete()
  deleteUser(@Body() credentials: Credentials): Bluebird<void> {
    return this.accountRepo.findUserByUsername(credentials.username)
      .then((user) => {
        this.accountRepo.remove(user);
      })
      .catch(this.internalErrorHandler);
  }

  @Put()
  editUser(@Body() userInfo: UserAccount): Bluebird<void> {
  
    if (!userInfo.username) {
      return Bluebird.reject(new BadRequestError('User info incorrect.'));
    }
    return this.accountRepo.findUserByUsername(userInfo.username)
      .then((user) => {
        delete userInfo.username;
        delete userInfo.password;
        user.update(userInfo.get({ plain:true }));
      })
      .catch(this.internalErrorHandler);
  }

  @Put('password')
  changePassword(@Body() credentials: Credentials): Bluebird<void> {
    return this.accountRepo.findUserByUsername(credentials.username)
      .then((user) => {
        user.password = credentials.password;
        user.save();
      })
      .catch(this.internalErrorHandler);
  }

  @Get(':id')
  getUserById(@Param('id') userId: NumberID) {
    return this.accountRepo.findUserById(userId)
      .catch((err) => {
        throw new ForbiddenError('err');
      });
  }

  private internalErrorHandler(err:any) {
    throw new InternalServerError(err);
  }
}
