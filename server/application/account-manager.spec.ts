import ava from 'ava';
import { AccountManagerApp } from './account-manager.app';
import { Credentials } from '../domain/auth/interface/login-params.type';
import { mockConfig } from '../config/dbconfig.mock';
import { dbConectionFactory } from '../domain/+shared/entity/db.adapter';
import { AccountRepository } from '../infrastructure/repository/account.repository';
import { UserAccount } from '../domain/account/entity/user-account.entity';

// Prepare
const test = ava;
// mockConfig.storage = '../cache/test.sqlite3';
mockConfig.logging = false;
const dbConnector = dbConectionFactory(mockConfig);

// Target
let accoutManager: AccountManagerApp;
const testUsername = 'stylethewalker_007';

// Object to test
const loginInfoToTest: Credentials = {
  username: 'user_1',
  password: '123456',
};

const loginInfoToTest2: Credentials = {
  username: 'user_2',
  password: 'abcd',
};


test.before('AccountRepository Module', (t) => {

  return dbConnector.sync().then(() => {
    accoutManager = new AccountManagerApp(new AccountRepository());

    return t.pass('Database connected');
  })
  .catch((err: any) => {

    return t.fail(err);
  });

});

test.serial('accountManager regists user: success', (t) => {
  return t.notThrows(accoutManager.createUser(loginInfoToTest));
});

test.serial('accountManager regists second user: success', (t) => {
  return t.notThrows(accoutManager.createUser(loginInfoToTest2));
});

test.serial('accountManager avoid duplicated registration: error', (t) => {
  return t.throws(accoutManager.createUser(loginInfoToTest));
});

test.serial('accountManager editsUser: error', (t) => {
  const userInfo = new UserAccount();
  return t.throws(accoutManager.editUser(userInfo));
});

test.serial('accountManager editsUser: success', (t) => {
  const userInfo = new UserAccount();
  userInfo.id = 0;
  userInfo.username = 'user_2';
  return t.notThrows(accoutManager.editUser(userInfo));
});

test.serial('accountManager getUserById: success', (t) => {
  return t.notThrows(accoutManager.getUserById(1));
});

test.serial('accountManager getUserById: error', (t) => {
  return t.throws(accoutManager.getUserById(10));
});

test.serial('accountManager changePassword: success', (t) => {
  const newUserInfo = loginInfoToTest;
  newUserInfo.password = 'iojojoijioij';
  return t.notThrows(accoutManager.changePassword(newUserInfo));
});

test.serial('accountManager changePassword: error', (t) => {
  const newUserInfo = {
    username: 'invalid_user',
    password: 'iojojoijioij',
  };

  return t.throws(accoutManager.changePassword(newUserInfo));
});

test.serial('accountManager deleteUser: success', (t) => {
  return t.notThrows(accoutManager.deleteUser(loginInfoToTest));
});

test.serial('accountManager deleteUser: error', (t) => {
  return t.throws(accoutManager.deleteUser(loginInfoToTest));
});
