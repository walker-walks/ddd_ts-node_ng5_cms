import { User } from '../../domain/+shared/entity/user.mapper';
import { Credentials } from '../../domain/auth/interface/login-params.type';
import * as Bluebird from 'bluebird';
import { NumberID, StringID } from '../../domain/+shared/types/generic.type';
import {
  IAccountRepository,
  accountRepositoryService,
} from '../../domain/account/interface/account-repository.interface';
import { Service } from 'typedi';
import { UserAccount } from '../../domain/account/entity/user-account.entity';
import { IAccount } from '../../domain/account/interface/account.interface';

// add, delte and finder methods
@Service(accountRepositoryService)
export class AccountRepository implements IAccountRepository {

  add(account: IAccount): Bluebird<void> {
    return User.create(account.get({ plain:true }))
    .then((user) => {})
    .catch((err) => {
      throw new Error('Registration Error. [Acc:01]');
    });
  }

  remove(account: IAccount): Bluebird<void> {
    return account.destroy();
  }

  findUserById(id: NumberID): Bluebird<UserAccount> {
    return User.findById(id)
      .then(this.nullRejector);
  }

  findUserByUsername(username: StringID): Bluebird<UserAccount> {
    return User.findOne({ where: { username } })
      .then(this.nullRejector);
  }

  private nullRejector(user: User | null): UserAccount {
    if (!user) {
      throw new Error('No user found. [Acc:04]');
    }

    return <UserAccount>user;
  }
}


