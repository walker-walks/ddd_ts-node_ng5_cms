import ava from 'ava';
import { mockConfig } from '../../config/dbconfig.mock';
import { AccountRepository } from './account.repository';
import { UserAccount } from '../../domain/account/entity/user-account.entity';
import { dbConectionFactory } from '../../domain/+shared/entity/db.adapter';

const test = ava;
// mockConfig.storage = '../cache/test.sqlite3';
mockConfig.logging = false;
const dbConnector = dbConectionFactory(mockConfig);
let accountRepo: AccountRepository;
let user: UserAccount;
const testUsername = 'stylethewalker';

test.before('AccountRepository Module', (t) => {

  return dbConnector.sync().then(() => {
    accountRepo = new AccountRepository();
    user = new UserAccount();
    user.nickname = 'koji';
    user.password = 'wow';
    user.username = testUsername;

    return t.pass('Database connected');
  })
  .catch((err: any) => {

    return t.fail(err);
  });

});

test.serial('accountRepo add function: add UserAccount', (t) => {
  return t.notThrows(accountRepo.add(user));
});

test.serial('accountRepo add function: add second UserAccount', (t) => {
  const secondUser = user;
  secondUser.username = 'second_repo user';
  return t.notThrows(accountRepo.add(secondUser));
});

test.serial('accountRepo add function: do not Add duplicate UserAccount', (t) => {
  return t.throws(accountRepo.add(user), 'Registration Error. [Acc:01]');
});

test.serial('accountRepo findByUsername function', (t) => {

  const findUser = accountRepo.findUserByUsername(testUsername)
  .then((account) => {
    if (account.username === testUsername) {
      return t.pass('user found correctly');
    }
    throw new Error(`wrong username: [${account.id}] ${account.username}`);
  });
  
  return t.notThrows(findUser);
});

test.serial('accountRepo findById valid ID', (t) => {
  return t.notThrows(accountRepo.findUserById(1));
});

test.serial('accountRepo findById invalid ID', (t) => {
  return t.throws(accountRepo.findUserById(10));
});

test.serial('accountRepo remove userAccount function', (t) => {
  const deleteUser = accountRepo.findUserByUsername(testUsername)
  .then((account) => {
    if (account.username !== testUsername) {
      throw new Error(`wrong username: [${account.id}] ${account.username}`);
    }
    return accountRepo.remove(account);
  });
  
  return t.notThrows(deleteUser);
});
