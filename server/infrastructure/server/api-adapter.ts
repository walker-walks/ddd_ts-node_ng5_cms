import { useExpressServer, useContainer } from 'routing-controllers';
import { Application } from 'express';
import { Container } from 'typedi';

useContainer(Container);

export const apiAdapter = (app: Application) => {
  useExpressServer(app, {
    routePrefix: '/api/',
    classTransformer: false,
    controllers: [process.cwd() + '/**/*.app.ts'],
  });
};
