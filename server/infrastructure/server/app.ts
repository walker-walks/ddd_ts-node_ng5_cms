// Settings
import { apiAdapter } from './api-adapter';
// express and plugins
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { Sequelize } from 'sequelize-typescript';
import { IServerConfig } from './interface/server.interface';


export class App {

  public app: express.Application;

  constructor(
    private config: IServerConfig,
    private dbConnection: Sequelize,
  ) {
    const app = express();

    // app config
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    apiAdapter(app);
    this.app = app;
  }

  serve() {
    this.dbConnection.sync().then(() => {
      this.app.listen(this.config.port, this.greet);
      console.log(
        `Server is listening on port: ${this.config.port}`,
      );
    })
    .catch((err) => {
      console.error('[DB Error] could not initializng DB...');
      console.error(err);
      console.error('---------- [EOM] --------');
    });
  }

  private greet(err: any) {
    if (err) {
      return console.error(err);
    }
  }
}
