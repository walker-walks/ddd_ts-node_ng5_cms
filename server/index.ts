// General Imports
import 'reflect-metadata';
// Settings
import { App } from './infrastructure/server/app';
import { SERVERCONFIG, DBCONFIG } from './config/dbconfig';
import { dbConectionFactory } from './domain/+shared/entity/db.adapter';

// Dev Settings
if (!SERVERCONFIG.isProdMode) {
  console.log('Server Mode: Dev');
}
const dbConnector = dbConectionFactory(DBCONFIG);
const app = new App(SERVERCONFIG, dbConnector);

app.serve();
