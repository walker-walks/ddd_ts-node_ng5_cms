import { ISequelizeConfig } from 'sequelize-typescript';

export let mockConfig: ISequelizeConfig = {
  host: process.env.DB_HOST || 'localhost',
  database: process.env.DB_NAME || 'cms_store',
  username: 'root',
  password: '',
  dialect: 'sqlite',
  storage: ':memory:',
  // logging: !process.env.DB_LOG,
};
