'use strict';

let defaultConfig = {
  apps : [{
    name        : 'server',
    script      : './index.ts',
    watch       : true,
    error_file  : 'logs/service-debug.error.log',
    out_file    : 'logs/service-debug.out.log',
    pid_file    : 'logs/service-debug.pid',
    log_date_format: '',
    ignore_watch: ['[\\/\\\\]\\./', 'node_modules', 'public', '.git', '.tscache', 'logs']
  }]
};

if ( process.platform === 'win32' ) {
  defaultConfig.apps[0].watch_options = { usePolling: true };
}
if ( process.env.NODE_ENV != 'production') {
  defaultConfig.apps[0].source_map_support = true;
  defaultConfig.apps[0].node_args =  ['--inspect=5858', '--cache-directory', '.tscache'];
}
if ( process.env.NODE_ENV === 'production') {
  defaultConfig.apps[0].node_args =  ['--fast'];
}

module.exports = defaultConfig;
