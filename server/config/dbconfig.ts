import { ISequelizeConfig } from 'sequelize-typescript';
import { IServerConfig } from '../infrastructure/server/interface/server.interface';

class ServerConfig implements IServerConfig {
  readonly port = process.env.PORT || '3000';
  readonly env = process.env.NODE_ENV || 'development';
  readonly isProdMode = process.env.NODE_ENV === 'production';
}

export const DBCONFIG: ISequelizeConfig = {
  host: process.env.DB_HOST || 'localhost',
  database: process.env.DB_NAME || 'cms_store',
  dialect:  process.env.DB_TYPE || 'mysql',
  username: process.env.DB_USER || 'app',
  password: process.env.DB_PASS || '_password',
  port: +(process.env.DB_PORT || 3306),
  logging: !process.env.DB_LOG,
  storage: ':memory:',
};

export const SERVERCONFIG = new ServerConfig();
