# Ollie

## Getting Started

1. [Download Go 1.8](https://golang.org/dl/)
1. [Install Go](https://golang.org/doc/install#install)
1. [Install Python 2.7](https://www.python.org/downloads/)
1. [Install GoogleCloud SDK](https://cloud.google.com/appengine/docs/standard/go/download)
1. [Install direnv](https://github.com/direnv/direnv)
1. [Install glide](https://github.com/Masterminds/glide)

Open terminal and enter...
```terminal
cd ollie-prepayed/src
glide up
cd ..
goapp serve app.yaml
```

Access to `http://localhost:8080/` in your browser.

## Documentation

1. [Official GAE/Go Standard Environment](https://cloud.google.com/appengine/docs/standard/go/)
1. [Official Docker image](https://hub.docker.com/r/google/cloud-sdk/)
