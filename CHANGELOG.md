# Change Log


## Semantic Versioning
https://semver.org/lang/pt-BR/


## [v0.1.1] - 2018-02-26
> Server: fix - data column types
- Server: added - nodemom for windows user
- Server: added - sqlite start option

## [v0.1.0] - 2018-02-25
> Server: added - account repository
- Server: added - account model
- Server: added - account manager app

## [v0.0.3] - 2018-02-25
> Server: added - test and coverage framework

## [v0.0.2] - 2018-02-17
> Server: added Server Skeleton
- Server: added server
- Server: added pm2 configuration
- Server: added docker file (node)
- Server: added docker file (mariadb)
- Server: setup log files for docker and local use
- Server: added debugger settings
- Server: added utils for managing ports
- Server: added workspace and editor config

## [v0.0.1] - 2018-02-16
> FIRST COMMIT
- added Client
- HMR module
- airbnb TypeScript linter
- Contributing guid
- Change log